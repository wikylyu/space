import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-qrcode-dialog',
  templateUrl: './qrcode-dialog.component.html',
  styleUrls: ['./qrcode-dialog.component.scss']
})
export class QRCodeDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<QRCodeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public value: string) {}


  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
