import { Component, OnInit } from '@angular/core';
import { SSService } from '../service/ss.service';


@Component({
  selector: 'app-user-dashboard-content',
  templateUrl: './user-dashboard-content.component.html',
  styleUrls: ['./user-dashboard-content.component.scss']
})
export class UserDashboardContentComponent implements OnInit {

  constructor(private ssService: SSService) { }

  ngOnInit() {
  }
}
