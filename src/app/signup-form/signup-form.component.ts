import { Component, OnInit, Inject } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { AccountService } from '../service/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss'],
})
export class SignupFormComponent implements OnInit {

  emailFC = new FormControl('', [Validators.required, Validators.email]);
  loading = false;
  password1 = '';
  password2 = '';
  email = '';

  getErrorMessage() {
    return this.emailFC.hasError('required') ? '请输入邮箱' :
        this.emailFC.hasError('email') ? '邮箱格式不正确' :
            '';
  }


  constructor(public snackBar: MatSnackBar, @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    private accountService: AccountService, private router: Router) {}

  ngOnInit() {
    const email = this.storage.get('user.email');
    if (email) {
      this.email = email;
    }
  }

  onSubmit() {
    if ( this.loading || !this.email || !this.emailFC.valid || !this.password1 || !this.password2) {
      return;
    } else if (this.password1 !== this.password2) {
      this.snackBar.open('密码不一致', '确定', {duration: 3000});
      return;
    }
    this.storage.set('user.email', this.email);
    this.loading = true;
    this.accountService.Signup(this.email, this.password1).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/user']);
    }, err => {
      this.loading = false;
      if (err.error.code === 103) {
        this.snackBar.open('密码长度不合法', '确定', {duration: 3000});
      } else if (err.error.code === 104) {
        this.snackBar.open('邮箱格式不合法', '确定', {duration: 3000});
      } else if (err.error.code === 100) {
        this.snackBar.open('邮箱已注册，请直接登录', '确定', {duration: 3000}).onAction().subscribe(() => {
          this.router.navigate(['/login']);
        });
      }
    });
  }

}
