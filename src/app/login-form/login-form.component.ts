import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Router } from '@angular/router';
import { AccountService } from '../service/account.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {

  emailFC = new FormControl('', [Validators.required, Validators.email]);
  loading = false;
  email = '';
  password = '';

  getErrorMessage() {
    return this.emailFC.hasError('required') ? '请输入邮箱' :
        this.emailFC.hasError('email') ? '邮箱格式不正确' :
            '';
  }


  constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService, private router: Router, private accountService: AccountService,
    private snackBar: MatSnackBar) {}

  ngOnInit() {
    const email = this.storage.get('user.email');
    if (email) {
      this.email = email;
    }
  }


  onSubmit() {
    if (this.loading || !this.email || !this.emailFC.valid || !this.password) {
      return;
    }
    this.accountService.Login(this.email, this.password).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/user']);
    }, err => {
      this.loading = false;
      if (err.error.code === 101) {
        this.snackBar.open('用户不存在，请先注册', '确认', {duration: 3000}).onAction().subscribe(() => {
          this.router.navigate(['/signup']);
        });
      } else if (err.error.code === 102) {
        this.snackBar.open('密码错误', '确定', {duration: 3000});
      }
    });
    this.storage.set('user.email', this.email);
    this.loading = true;
  }
}
