import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { UserPageComponent } from './user-page/user-page.component';
import { UserDashboardContentComponent } from './user-dashboard-content/user-dashboard-content.component';
import { UserProfileContentComponent } from './user-profile-content/user-profile-content.component';
import { UserNodeContentComponent } from './user-node-content/user-node-content.component';
import { UserFaqContentComponent } from './user-faq-content/user-faq-content.component';
import { UserProductContentComponent } from './user-product-content/user-product-content.component';


const routes: Routes = [
    {
        path: '',
        component: MainPageComponent,
        children: [
            {
                path: 'login',
                component: LoginFormComponent,
            },
            {
                path: 'signup',
                component: SignupFormComponent,
            }
        ]
    },
    {
        path: 'user',
        component: UserPageComponent,
        children: [
            {
                path: '',
                component: UserDashboardContentComponent,
            },
            {
                path: 'profile',
                component: UserProfileContentComponent,
            },
            {
                path: 'node',
                component: UserNodeContentComponent,
            },
            {
                path: 'product',
                component: UserProductContentComponent,
            },
            {
                path: 'faq',
                component: UserFaqContentComponent,
            }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {enableTracing:true})],
    exports: [RouterModule]
})

export class AppRoutingModule {}
