import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  private clickCount:number = 0;

  constructor(private router: Router) {}

  ngOnInit() {
  }

  onSpaceClick(){
    this.clickCount += 1;
    if(this.clickCount>=5){
      this.clickCount=0;
      this.router.navigate(['/login']);
      console.log(this.clickCount);
    }
  }

}
