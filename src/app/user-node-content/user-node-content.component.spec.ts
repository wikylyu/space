import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserNodeContentComponent } from './user-node-content.component';

describe('UserNodeContentComponent', () => {
  let component: UserNodeContentComponent;
  let fixture: ComponentFixture<UserNodeContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserNodeContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserNodeContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
