import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '../animation/fadein';
import { SSService } from '../service/ss.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { QRCodeDialogComponent } from '../qrcode-dialog/qrcode-dialog.component';

@Component({
  selector: 'app-user-node-content',
  templateUrl: './user-node-content.component.html',
  styleUrls: ['./user-node-content.component.scss'],
})
export class UserNodeContentComponent implements OnInit {

  public nodes: any;
  public ssinfo: any;
  public ssStatus = '未开通';

  constructor(private snackBar: MatSnackBar, private dialog: MatDialog, private ssService: SSService) { }

  ngOnInit() {
    this.ssService.FindNodes().subscribe(data => {
      this.nodes = data;
    });
    this.ssService.GetUserSSInfo().subscribe(data => {
      this.ssinfo = data;
    }, err => {
      if (err.error.code !== 200) {
        this.snackBar.open('网络错误', '确认', {duration: 3000});
      }
    });
    this.ssService.GetCurrentUserService().subscribe(data => {
      if (data.status === this.ssService.ServiceStatusWaiting) {
        this.ssStatus = '未开始';
      } else if (data.status === this.ssService.ServiceStatusRunning) {
        this.ssStatus = '正常';
      } else if (data.status === this.ssService.ServiceStatusFinished) {
        this.ssStatus = '已过期';
      } else if (data.status === this.ssService.ServiceStatusLimited) {
        this.ssStatus = '流量限制';
      }
    });
  }

  showQRCode(n) {
    const ssinfo = this.ssinfo;
    const url = 'ss://' + btoa(ssinfo.method + ':' + ssinfo.password + '@' + n.public_host + ':' + ssinfo.port);
    this.dialog.open(QRCodeDialogComponent, {data: url});
  }

}
