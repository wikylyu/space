import { Injectable, Inject } from '@angular/core';
import { HttpService } from './http.service';
import { ConfigService } from './config.service';

@Injectable()
export class AccountService {

    constructor(private http: HttpService) {}

    Login(email, password) {
        const data = {
          email: email,
          password: password,
        };
        return this.http.put(ConfigService.Host + '/account/token', data);
    }

    Logout() {
        return this.http.delete(ConfigService.Host + '/account/token');
    }

    GetAccountInfo() {
        return this.http.get(ConfigService.Host + '/account');
    }

    Signup(email, password) {
        const data = {
            email: email,
            password: password,
          };
          return this.http.post(ConfigService.Host + '/account', data);
    }
}
