import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ConfigService {
  static Host: string;
  static Source: string;

  static DevHost = 'http://127.0.0.1:2313/api';
  static DevSource = '38beb27d-098b-41e4-8306-8175b05f9992';

  static ProdHost = 'https://quasar.wikylyu.xyz/api';
  static ProdSource = '38beb27d-098b-41e4-8306-8175b05f9992';
}

if (environment.production) {
  ConfigService.Host = ConfigService.ProdHost;
  ConfigService.Source = ConfigService.ProdSource;
} else {
  ConfigService.Host = ConfigService.DevHost;
  ConfigService.Source = ConfigService.DevSource;
}
