import { Injectable, Inject } from '@angular/core';
import { HttpService } from './http.service';
import { ConfigService } from './config.service';

@Injectable()
export class SSService {
    public ServiceStatusWaiting  = 0;
    public ServiceStatusRunning  = 1;
    public ServiceStatusFinished = 2;
    public ServiceStatusLimited  = 3;

    constructor(private http: HttpService) {}

    FindNodes() {
        const url = ConfigService.Host + '/ss/nodes';
        return this.http.get(url);
    }

    GetUserSSInfo() {
        const url = ConfigService.Host + '/ss/info';
        return this.http.get(url);
    }

    GetCurrentUserService() {
        const url = ConfigService.Host + '/ss/service/current';
        return this.http.get(url);
    }

    FindUserHourlyFlowStats(hours: number = 24) {
        const url = ConfigService.Host + '/ss/flow/stats?period=hourly&hours=' + hours;
        return this.http.get(url);
    }

    FindUserDailyFlowStats(days: number = 30) {
        const url = ConfigService.Host + '/ss/flow/stats?period=daily&days=' + days;
        return this.http.get(url);
    }

    FindUserMonthlyFlowStats(months: number = 6) {
        const url = ConfigService.Host + '/ss/flow/stats?period=monthly&months=' + months;
        return this.http.get(url);
    }

}
