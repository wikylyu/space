import { Injectable, Inject } from '@angular/core';
import { HttpService } from './http.service';
import { ConfigService } from './config.service';

@Injectable()
export class ProductService {

    constructor(private http: HttpService) {}

    FindProducts() {
        return this.http.get(ConfigService.Host + '/products');
    }

    CreateOrder(productID) {
        const data = {
            product_ids: [productID],
        };
        return this.http.post(ConfigService.Host + '/order', data);
    }

    PayOrder(orderID) {
        return this.http.get(ConfigService.Host + '/pay/order/' + orderID);
    }
}
