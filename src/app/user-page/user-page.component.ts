import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../service/account.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  @ViewChild('drawer') drawer;
  public path: string;

  constructor(private router: Router, private accountService: AccountService) { }


  ngOnInit() {
    this.path = this.router.url;

    this.accountService.GetAccountInfo().subscribe(() => {}, err => {
      this.router.navigate(['/login']);
    });
  }

  onMenuChange(path) {
    this.path = path;
    this.router.navigate([path]);
    // this.drawer.toggle();
  }

  onLogout() {
    this.accountService.Logout().subscribe(() => {
      this.router.navigate(['/login']);
    });
  }

}
