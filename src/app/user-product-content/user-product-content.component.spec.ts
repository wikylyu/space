import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProductContentComponent } from './user-product-content.component';

describe('UserProductContentComponent', () => {
  let component: UserProductContentComponent;
  let fixture: ComponentFixture<UserProductContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProductContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProductContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
