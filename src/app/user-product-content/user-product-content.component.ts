import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-user-product-content',
  templateUrl: './user-product-content.component.html',
  styleUrls: ['./user-product-content.component.scss']
})
export class UserProductContentComponent implements OnInit {

  public products: any;

  constructor(private productService: ProductService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.productService.FindProducts().subscribe(data => {
      this.products = data;
    });
  }

  onBuy(productID) {
    console.log(productID);
    this.productService.CreateOrder(productID).subscribe(data => {
      this.productService.PayOrder((<any>data).id).subscribe(() => {
        this.snackBar.open('支付成功', '确认', {duration: 3000});
      });
    });
  }

}
