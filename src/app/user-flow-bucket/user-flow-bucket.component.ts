import { Component, OnInit } from '@angular/core';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { SSService } from '../service/ss.service';


@Component({
  selector: 'app-user-flow-bucket',
  templateUrl: './user-flow-bucket.component.html',
  styleUrls: ['./user-flow-bucket.component.scss']
})
export class UserFlowBucketComponent implements OnInit {

  private chart: AmChart;

  constructor(private AmCharts: AmChartsService, private ssService: SSService) { }

  ngOnInit() {
    this.InitChart();
    this.getServiceLimit();
  }

  InitChart() {
    const chartData = [ {
      category: '剩余流量',
      value1: 0,
      value2: 100,
      left: '0 MB',
      used: '0 MB',
    } ];
    this.chart = this.AmCharts.makeChart('user-flow-bucket', {
      theme: 'light',
      type: 'serial',
      depth3D: 100,
      angle: 30,
      autoMargins: true,
      dataProvider: chartData,
      valueAxes: [ {
        stackType: '100%',
        gridAlpha: 0
      } ],
      graphs: [ {
        type: 'column',
        balloonText: `剩余流量[[left]]`,
        topRadius: 1,
        columnWidth: 0.5,
        showOnAxis: true,
        lineThickness: 2,
        lineAlpha: 0.5,
        lineColor: '#FFFFFF',
        fillColors: '#2196F3',
        fillAlphas: 0.8,
        valueField: 'value1',
      }, {
        type: 'column',
        balloonText: `已用流量[[used]]`,
        topRadius: 1,
        columnWidth: 0.5,
        showOnAxis: true,
        lineThickness: 2,
        lineAlpha: 0.5,
        lineColor: '#cdcdcd',
        fillColors: '#cdcdcd',
        fillAlphas: 0.5,
        valueField: 'value2'
      } ],
      categoryField: 'category',
      categoryAxis: {
        axisAlpha: 0,
        labelOffset: 40,
        gridAlpha: 0
      },
    });
  }

  OnDestroy() {
    if (this.chart) {
      this.AmCharts.destroyChart(this.chart);
    }
  }

  getServiceLimit() {
    this.ssService.GetCurrentUserService().subscribe(data => {
      const d = <any>data;

      const left = parseFloat(((d.flow_limit - d.flow_used) / 1024.0 / 1024.0).toFixed(2));
      const used = parseFloat(((d.flow_used) / 1024.0 / 1024.0).toFixed(2));


      const chartData = [ {
        category: '当前流量使用情况',
        value1: (d.flow_limit - d.flow_used) / d.flow_limit,
        value2: d.flow_used / d.flow_limit,
        left: left + ' MB',
        used: used + ' MB',
      } ];
      this.AmCharts.updateChart(this.chart, () => {
        this.chart.dataProvider = chartData;
      });
    });
  }
}
