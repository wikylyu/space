import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFlowBucketComponent } from './user-flow-bucket.component';

describe('UserFlowBucketComponent', () => {
  let component: UserFlowBucketComponent;
  let fixture: ComponentFixture<UserFlowBucketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFlowBucketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFlowBucketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
