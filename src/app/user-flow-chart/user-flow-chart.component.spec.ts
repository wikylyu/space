import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFlowChartComponent } from './user-flow-chart.component';

describe('UserFlowChartComponent', () => {
  let component: UserFlowChartComponent;
  let fixture: ComponentFixture<UserFlowChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFlowChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFlowChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
