import { Component, OnInit } from '@angular/core';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { SSService } from '../service/ss.service';

@Component({
  selector: 'app-user-flow-chart',
  templateUrl: './user-flow-chart.component.html',
  styleUrls: ['./user-flow-chart.component.scss']
})
export class UserFlowChartComponent implements OnInit {

  private chart: AmChart;
  public loading = false;

  constructor(private AmCharts: AmChartsService, private ssService: SSService) { }

  ngOnInit() {
    this.InitChart();
    this.getHourlyFlowStat(25);
  }

  InitChart() {
    this.chart = this.AmCharts.makeChart('user-flow-chart', {
      type: 'serial',
      theme: 'light',
      marginTop: 10,
      marginRight: 80,
      dataProvider: [],
      valueAxes: [{
        axisAlpha: 0,
        position: 'left',
        unit: 'MB',
        axisColor: '#3f51b5',
      }, {
        axisAlpha: 0,
        position: 'right',
        unit: 'MB',
        axisColor: '#009688',
      }],
      graphs: [{
          id: 'g1',
          balloonText: `下行流量 [[key]]<br><b><span style='font-size:14px;'>[[egress]]MB</span></b>`,
          bullet: 'point',
          bulletSize: 8,
          lineColor: '#3f51b5',
          lineThickness: 2,
          type: 'smoothedLine',
          valueField: 'egress'
      }, {
        id: 'g2',
        balloonText: `上行流量 [[key]]<br><b><span style='font-size:14px;'>[[ingress]]MB</span></b>`,
        bullet: 'point',
        bulletSize: 8,
        lineColor: '#009688',
        lineThickness: 2,
        type: 'smoothedLine',
        valueField: 'ingress'
    }],
      chartCursor: {
          cursorAlpha: 0,
          valueLineEnabled: true,
          valueLineBalloonEnabled: true,
          valueLineAlpha: 0.5,
          fullWidth: true
      },
      categoryField: 'time',
    });
  }

  getHourlyFlowStat(hours: number) {
    this.loading = true;
    this.ssService.FindUserHourlyFlowStats(hours).subscribe(data => {
      this.loading = false;
      this.updateHourlyFlowStat(hours, data);
    }, () => {
      this.loading = false;
    });
  }

  updateHourlyFlowStat(hours: number, data) {
    const values = [];
    const stats = {};
    data.forEach(e => {
      const key = e.year + '/' + e.month + '/' + e.day + ':' + e.hour;
      stats[key] = e;
    });
    for (let i = hours; i >= 0; i-- ) {
      const t = new Date();
      t.setUTCHours(t.getUTCHours() - i);
      const year = t.getFullYear();
      const month = t.getMonth() + 1;
      const day = t.getDate();
      const hour = t.getHours();
      const key = year + '/' + month + '/' + day + ':' + hour;
      const stat = stats[key];
      let time = year + '/' + month + '/' + day  ;
      let ingress = 0.0;
      let egress = 0.0;
      if ( hour !== 0 ) {
        time = '' + hour;
      }
      if ( stat ) {
        ingress = parseFloat(((stat.ingress ) / 1024.0 / 1024.0).toFixed(2));
        egress = parseFloat(((stat.egress ) / 1024.0 / 1024.0).toFixed(2));
      }
      values.push({
        key: year + '/' + month + '/' + day + ' ' + hour + ':00:00',
        time: time,
        ingress: ingress,
        egress: egress,
      });
    }

    this.AmCharts.updateChart(this.chart, () => {
      this.chart.dataProvider = values;
    });
  }


  getDailyFlowStat() {
    this.loading = true;
    this.ssService.FindUserDailyFlowStats(31).subscribe(data => {
      this.loading = false;
      this.updateDailyFlowStat(data);
    }, () => {
      this.loading = false;
    });
  }

  updateDailyFlowStat(data) {
    const values = [];
    const stats = {};
    data.forEach(e => {
      const key = e.year + '/' + e.month + '/' + e.day;
      stats[key] = e;
    });
    for (let i = 31; i >= 0; i-- ) {
      const t = new Date();
      t.setUTCDate(t.getUTCDate() - i);
      const year = t.getFullYear();
      const month = t.getMonth() + 1;
      const day = t.getDate();
      const key = year + '/' + month + '/' + day;
      const stat = stats[key];
      let time = year + '/' + month;
      let ingress = 0.0;
      let egress = 0.0;
      if ( day !== 1 ) {
        time = '' + day;
      }
      if ( stat ) {
        ingress = parseFloat(((stat.ingress ) / 1024.0 / 1024.0).toFixed(2));
        egress = parseFloat(((stat.egress ) / 1024.0 / 1024.0).toFixed(2));
      }
      values.push({
        key: year + '/' + month + '/' + day,
        time: time,
        ingress: ingress,
        egress: egress,
      });
    }

    this.AmCharts.updateChart(this.chart, () => {
      this.chart.dataProvider = values;
    });
  }

  getMonthlyFlowStat() {
    this.loading = true;
    this.ssService.FindUserMonthlyFlowStats(12).subscribe(data => {
      this.loading = false;
      this.updateMonthlyFlowStat(data);
    }, () => {
      this.loading = false;
    });
  }

  updateMonthlyFlowStat(data) {
    const values = [];
    const stats = {};
    data.forEach(e => {
      const key = e.year + '/' + e.month;
      stats[key] = e;
    });
    for (let i = 12; i >= 0; i-- ) {
      const t = new Date();
      t.setUTCMonth(t.getUTCMonth() - i);
      const year = t.getFullYear();
      const month = t.getMonth() + 1;
      const key = year + '/' + month ;
      const stat = stats[key];
      let time = year + '/' + month;
      let ingress = 0.0;
      let egress = 0.0;
      if ( month !== 1 ) {
        time = '' + month;
      }
      if ( stat ) {
        ingress = parseFloat(((stat.ingress ) / 1024.0 / 1024.0).toFixed(2));
        egress = parseFloat(((stat.egress ) / 1024.0 / 1024.0).toFixed(2));
      }
      values.push({
        key: year + '/' + month,
        time: time,
        ingress: ingress,
        egress: egress,
      });
    }

    this.AmCharts.updateChart(this.chart, () => {
      this.chart.dataProvider = values;
    });
  }

  OnDestroy() {
    if (this.chart) {
      this.AmCharts.destroyChart(this.chart);
    }
  }

  onChange(e) {
    if ( this.loading ) {
      return;
    }
    if (e.value === '24hour') {
      this.getHourlyFlowStat(25);
    } else if (e.value === '48hour') {
      this.getHourlyFlowStat(49);
    } else if (e.value === 'month') {
      this.getDailyFlowStat();
    } else if (e.value === 'year') {
      this.getMonthlyFlowStat();
    }
  }
}
