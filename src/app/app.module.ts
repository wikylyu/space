import 'hammerjs';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { StorageServiceModule } from 'angular-webstorage-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AmChartsModule } from '@amcharts/amcharts3-angular';
import { QRCodeModule } from 'angular2-qrcode';

import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { SpaceZoneComponent } from './space-zone/space-zone.component';
import { UserPageComponent } from './user-page/user-page.component';
import { UserDashboardContentComponent } from './user-dashboard-content/user-dashboard-content.component';
import { UserNodeContentComponent } from './user-node-content/user-node-content.component';
import { UserProfileContentComponent } from './user-profile-content/user-profile-content.component';
import { UserFaqContentComponent } from './user-faq-content/user-faq-content.component';
import { UserProductContentComponent } from './user-product-content/user-product-content.component';
import { HttpService } from './service/http.service';
import { RequestOptions, XHRBackend, HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { ConfigService } from './service/config.service';
import { AccountService } from './service/account.service';
import { ProductService } from './service/product.service';
import { SSService } from './service/ss.service';
import { QRCodeDialogComponent } from './qrcode-dialog/qrcode-dialog.component';
import { UserFlowChartComponent } from './user-flow-chart/user-flow-chart.component';
import { LoadingMaskComponent } from './loading-mask/loading-mask.component';
import { UserFlowBucketComponent } from './user-flow-bucket/user-flow-bucket.component';


@NgModule({
  exports: [
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],
})
export class MaterialModule {}

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    LoginFormComponent,
    SignupFormComponent,
    SpaceZoneComponent,
    UserPageComponent,
    UserDashboardContentComponent,
    UserNodeContentComponent,
    UserProfileContentComponent,
    UserFaqContentComponent,
    UserProductContentComponent,
    QRCodeDialogComponent,
    UserFlowChartComponent,
    LoadingMaskComponent,
    UserFlowBucketComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    StorageServiceModule,
    FormsModule,
    AmChartsModule,
    QRCodeModule,
    HttpClientModule,
    MaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  entryComponents: [QRCodeDialogComponent],
  providers: [
    ConfigService,
    AccountService,
    ProductService,
    SSService,
    {
      provide: HttpService,
      useFactory: (backend: XHRBackend, options: RequestOptions, router: Router) => {
        return new HttpService(backend, options, router);
      },
      deps: [XHRBackend, RequestOptions]
    }
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
