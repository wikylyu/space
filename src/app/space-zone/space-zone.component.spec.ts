import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaceZoneComponent } from './space-zone.component';

describe('SpaceZoneComponent', () => {
  let component: SpaceZoneComponent;
  let fixture: ComponentFixture<SpaceZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaceZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
