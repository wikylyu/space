import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFaqContentComponent } from './user-faq-content.component';

describe('UserFaqContentComponent', () => {
  let component: UserFaqContentComponent;
  let fixture: ComponentFixture<UserFaqContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFaqContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFaqContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
